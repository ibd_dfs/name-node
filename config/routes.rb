Rails.application.routes.draw do
  post 'user/init', to: 'file_metadata#init_user'

  post 'dir/mk', to: 'file_metadata#mk'
  get 'dir/ls', to: 'file_metadata#ls'
  delete 'dir/rm', to: 'file_metadata#delete'

  post 'node/register', to: 'node#register_node'
  post 'node/healthcheck', to: 'node#health_check'
  post 'node/file_created', to: "node#file_created"

  post 'file/create', to: 'file_metadata#create_file'
  post 'file/write', to: 'file_metadata#write_file'
  get 'file/read', to: 'file_metadata#read_file'
  delete 'file/delete', to: 'file_metadata#delete'
  get 'file/info', to: 'file_metadata#file_info'

  post 'file/move', to: 'file_metadata#move_file'
  post 'file/copy', to: 'file_metadata#copy_file'
end
