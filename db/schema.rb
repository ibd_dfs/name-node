# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_10_124404) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "file_metadata", force: :cascade do |t|
    t.bigint "size"
    t.string "name"
    t.string "full_path"
    t.boolean "is_directory"
    t.integer "parent_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "nodes", force: :cascade do |t|
    t.string "private_address"
    t.bigint "total_volume"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "public_address"
    t.datetime "last_alive"
    t.index ["private_address"], name: "index_nodes_on_private_address", unique: true
    t.index ["public_address"], name: "index_nodes_on_public_address", unique: true
  end

  create_table "replicas", force: :cascade do |t|
    t.bigint "node_id", null: false
    t.bigint "file_metadata_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["file_metadata_id"], name: "index_replicas_on_file_metadata_id"
    t.index ["node_id", "file_metadata_id"], name: "index_replicas_on_node_id_and_file_metadata_id", unique: true
    t.index ["node_id"], name: "index_replicas_on_node_id"
  end

  add_foreign_key "file_metadata", "file_metadata", column: "parent_id", on_delete: :cascade
  add_foreign_key "replicas", "file_metadata", column: "file_metadata_id", on_delete: :cascade
  add_foreign_key "replicas", "nodes", on_delete: :cascade
end
