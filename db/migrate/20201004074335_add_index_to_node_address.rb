class AddIndexToNodeAddress < ActiveRecord::Migration[6.0]
  def change
    add_index :nodes, :address, unique: true
  end
end
