class CreateNodes < ActiveRecord::Migration[6.0]
  def change
    create_table :nodes do |t|
      t.string :address
      t.integer :total_volume

      t.timestamps
    end
  end
end
