class AddPublicAddressToNode < ActiveRecord::Migration[6.0]
  def change
    rename_column :nodes, :address, :private_address

    add_column :nodes, :public_address, :string
  end
end
