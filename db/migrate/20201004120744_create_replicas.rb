class CreateReplicas < ActiveRecord::Migration[6.0]
  def change
    create_table :replicas do |t|
      t.belongs_to :node, null: false, foreign_key: { on_delete: :cascade }
      t.belongs_to :file_metadata, null: false, foreign_key: { on_delete: :cascade }

      t.timestamps
    end
  end
end
