class ChangeSizeColumnType < ActiveRecord::Migration[6.0]
  def change
    change_column :nodes, :total_volume, :bigint
    change_column :file_metadata, :size, :bigint
  end
end
