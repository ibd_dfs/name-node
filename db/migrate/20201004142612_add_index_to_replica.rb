class AddIndexToReplica < ActiveRecord::Migration[6.0]
  def change
    add_index :replicas, [:node_id, :file_metadata_id], unique: true
  end
end
