class CreateFileMetadata < ActiveRecord::Migration[6.0]
  def change
    create_table :file_metadata do |t|
      t.integer :size
      t.string :name
      t.string :full_path
      t.boolean :is_directory
      t.integer :parent_id

      t.timestamps
    end
  end
end
