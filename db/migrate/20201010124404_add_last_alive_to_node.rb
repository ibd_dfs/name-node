class AddLastAliveToNode < ActiveRecord::Migration[6.0]
  def change
    add_column :nodes, :last_alive, :timestamp
  end
end
