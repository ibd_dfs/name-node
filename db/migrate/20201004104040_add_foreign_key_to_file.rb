class AddForeignKeyToFile < ActiveRecord::Migration[6.0]
  def change
    add_foreign_key :file_metadata, :file_metadata, on_delete: :cascade, column: :parent_id
  end
end
