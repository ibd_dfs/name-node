class AddIndexToPublicAddress < ActiveRecord::Migration[6.0]
  def change
    add_index :nodes, :public_address, unique: true
  end
end
