# DS Project 2

## Launch

### Nameserver
1) Download [docker-compose-deploy.yml](https://gitlab.com/ibd_dfs/name-node/-/blob/master/docker-compose-deploy.yml)
2) Create an .env file in the same directory ([example](https://gitlab.com/ibd_dfs/name-node/-/blob/master/.env)) 
3) `docker-compose up` 

### Storage
#### On aws:
1) Download script `run.sh` (download from gitlab or simply clone repo)
2) Run script `run.sh` with two parameters: Nameserver host and capacity (in bytes)

Example: 

```bash
wget https://gitlab.com/ibd_dfs/storage-node/-/blob/master/run.sh
. run.sh http://10.0.0.15:3000 1000000000
```
#### On other vpc:
Run docker container with the following envs:
* public ip address of storage (for client)
* private ip address (for nameserver)
* nameserver host (for storage registration and healthcheck)
* initial capacity (in bytes)
* Path to the storage inside container (default is /var/storage)
Example: 

```bash
docker run -p 5000:5000 \
-e NAME_HOST=http://10.0.0.15:3000 \
-e PUBLIC_IP=http://188.130.155.151:5000 \
-e PRIVATE_IP=http://10.0.0.158:5000 \
-e SERVER_CAPACITY=1000000000 \
sgmakarov/storage
```
or put this variables into some file and run 

```bash
docker run -p 5000:5000 --env-file .env sgmakarov/storage
```
### Client

You can either run the code from sources or use a pre-built docker image.

#### Run from sources

Requirements:
* Python 3.8

Run
```
pip3 install -r requirements.txt
python3 main.py <nameserver address> <username>
```

#### Run via docker:

Requirements:
* Docker

##### Run from pre-built image.
```bash
docker run -i \
    -v "$(pwd)/storage":/app/storage \
    -v "$(pwd)/upload":/upload \
    winnerokay/dfs_client:latest \
    python main.py "$1" "$2" < "/dev/stdin"
    
# a command to get access to downloaded files
sudo chown -R "$USER" "$(pwd)/storage"

```


## Architecture
![](https://i.imgur.com/R8IhFxm.png)

## Communication protocols

![](https://i.imgur.com/oGwWoKT.png)

## Design decisions
* Name server does not notify storage nodes about new folders - they only 'flushed' when they have at least one effective file. It is done to reduce sync complexity
* To let nameserver know about replication status of the whole tree, when replica of some file is comitted, creation of parent folder replicas' metadata is triggered. This is done for efficient folder deletion lookup
* Deletion process is the same for both file and directory, the only difference is the storage endpoint
* Replication is pefrormed after node confirms file transfer/creation
* The file metadata is written once namenode recieves a confirmation of transferring start from storage node. However, replica info is written only after node confirms replication process' successfull end. The intermediate state can be considered as a "pending". For client it means, that file will be shown in the tree, but access attempt will result in "Still uploading" error
* Storage node clears all local files on startup. The namenode do the same for replica's metadata if the node trying to register is already exists in database 

## Challenges

* **The database structure, tried several options:**
    * Postgresql’ ltree - very efficient lookups, but does not support dots in file names
    * Rails’ library for managing tree structures - failed to setup
    * Just one table with self-referencing foreign key - the best option ever!


* **Client dockerization**
    * tty available vs smooth UX


* **Hard to merge all APIs due to some misunderstandings**


## Contribution

The distribution of work was fairly determined using https://www.random.org/ 

* **Valentin Sergeev**
    * Namenode
    * Architectural decisions

* **Sergey Makarov**
    * Storage nodes 
    * Architectural decisions
    * AWS deployment

* **Daniil Manakovskiy**
    * Client command line interface 
    * Architectural decisions


Commit history in repos can be considered as a proof of work

## Links

### Docker images
[Nameserver](https://hub.docker.com/r/valentun/ds-namenode)
[Storage node](https://hub.docker.com/repository/docker/sgmakarov/storage)
[Client](https://hub.docker.com/repository/docker/winnerokay/dfs_client)
### Sources
[Nameserver](https://gitlab.com/ibd_dfs/name-node)
[Storage node](https://gitlab.com/ibd_dfs/storage-node)
[Client](https://gitlab.com/ibd_dfs/client)