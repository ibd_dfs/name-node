require 'net/http'
require 'json'

class StorageNodeCommunicator
  @logger

  # @param node_address [String]
  def initialize(node_address, logger)
    @address = node_address
    @logger = logger
  end

  # @return [Boolean]
  # @param full_path [String]
  def send_create_file(full_path)
    body = {full_path: full_path}

    make_node_request('file/create/', body)
  end

  # @return [Boolean]
  def replicate_file(address_from, full_path)
    copy_file_from_neighbour(address_from, full_path, full_path)
  end

  # @return [Boolean]
  def copy_file_from_neighbour(address_from, full_path_from, full_path_to)
    body = {ip_from: address_from, full_path_from: full_path_from, full_path_to: full_path_to}

    make_node_request("file/copy/", body)
  end

  # @return [Boolean]
  # @param full_path [String]
  def file_delete(full_path)
    body = { full_path: full_path }

    make_node_request("file/delete/", body, :delete)
  end

  # @return [Boolean]
  def file_move(full_path_from, full_path_to)
    body = { full_path_from: full_path_from, full_path_to: full_path_to }

    make_node_request("file/move/", body)
  end

  # @return [Boolean]
  # @param full_path [String]
  def dir_delete(full_path)
    body = { full_path: full_path }

    make_node_request("dir/delete/", body, :delete)
  end

  def make_node_request(relative_url, body, method = :post)
    uri = URI("#{@address}/#{relative_url}")

    begin
      if method == :post
        req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
      elsif method == :delete
        req = Net::HTTP::Delete.new(uri, 'Content-Type' => 'application/json')
      else
        raise "Unknown method: #{method}"
      end

      req.body = body.to_json

      @logger.debug "\nSending [#{method.upcase}] #{req.body} to #{uri.to_s}"

      res = Net::HTTP.start(uri.hostname, uri.port) do |http|
        http.request(req)
      end

      @logger.debug "Received #{res.code} #{res.body}\n"

      res.code.to_i < 400
    rescue Exception => e
      @logger.error "\nFailed to send #{body.to_json} to #{uri.to_s}, reason: #{e.message}\n"

      false
    end
  end
end