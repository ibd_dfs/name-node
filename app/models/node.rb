class Node < ApplicationRecord
  validates :total_volume, presence: true
  validates :public_address, presence: true, uniqueness: true
  validates :private_address, presence: true, uniqueness: true

  has_many :replicas
  has_many :files, class_name: 'FileMetadata', through: :replicas, source: :file_metadata

  def free_space
    taken_size = files.where(is_directory: false).map(&:size).reject(&:nil?).reduce(0, :+)
    total_volume - taken_size
  end

  # @param file [FileMetadata]
  # @return [Node]
  def self.find_for_new_file(file)
    Node.all.map { |node| [node, node.free_space] }
        .filter { |compound| compound[1] > file.size }
        .sort_by { |compound| compound[1] }.reverse
        .map { |compound| compound[0] }
        .first
  end

  # Returns best nodes for replicating file.
  # Only the nodes with enough free space are considered
  # Only the nodes which dont have this file are considered
  # Only alive nodes are considered
  # The more free space node has, the better it suits
  #
  # @param file [FileMetadata]
  def self.replicate_candidates(file)
    current_replicas = file.replicas.size

    Node.find_by_sql("SELECT * FROM nodes WHERE id NOT IN
                      (SELECT node_id from replicas WHERE file_metadata_id = #{file.id})")
        .filter { |node| DateTime.now.to_i - node.last_alive.to_i < node_timeout }
        .map { |node| [node, node.free_space] }
        .filter { |compound| compound[1] > file.size }
        .sort_by { |compound| compound[1] }.reverse
        .map { |compound| compound[0] }
        .take(replication_factor - current_replicas)
  end

  def self.replication_factor
    ENV['REPLICATION_FACTOR'].to_i
  end

  def self.node_timeout
    ENV['NODE_TIMOUT'].to_i
  end

  def self.check_nodes_alive
    Node.all.filter { |node| DateTime.now.to_i - node.last_alive.to_i > node_timeout }
        .each { |node| ReplicateFilesJob.perform_now(node.id) }
  end
end
