class Replica < ApplicationRecord
  belongs_to :node
  belongs_to :file_metadata

  validates :file_metadata_id, uniqueness: { scope: :node_id }
end
