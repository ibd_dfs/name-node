class FileMetadata < ApplicationRecord
  belongs_to :parent, class_name: 'FileMetadata', foreign_key: 'parent_id', :optional => true
  has_many :children, class_name: 'FileMetadata', foreign_key: 'parent_id'

  has_many :replicas

  validate :should_have_unique_path
  validates :name, presence: true
  validate :should_have_enough_space

  # @return [Node]
  def source_node
    return nil if replicas.empty?

    replicas.order("RANDOM()").first.node
  end

  # @return [FileMetadata]
  def self.make_path(user, path)
    current_ancestor = get_file(user, "")

    return nil if current_ancestor.nil?

    segments = path.split("/")
    current_path = user

    segments.each do |segment|
      current_path += "/#{segment}"

      next_ancestor = FileMetadata.find_by_full_path(current_path)

      if next_ancestor.nil?
        next_ancestor = current_ancestor.children.create!(name: segment, full_path: current_path, is_directory: true, size: nil)
      end

      current_ancestor = next_ancestor
    end

    current_ancestor
  end

  # @return [FileMetadata]
  def self.new_directory(user, path, dir_name)
    parent = FileMetadata.find_by_full_path(user_path(user, path))

    if parent.nil?
      parent = make_path(user, path)
    end

    if parent.nil?
      return nil
    end

    parent.children.new(name: dir_name, full_path: full_path(user, path, dir_name), is_directory: true, size: nil)
  end

  # @return [FileMetadata]
  def self.create_empty_file(user, path, file_name)
    create_file(user, path, file_name, 0)
  end

  # @return [FileMetadata]
  def self.create_file(user, path, file_name, file_size)
    full_path = full_path(user, path, file_name)

    parent = FileMetadata.find_by_full_path(user_path(user, path))

    if parent.nil?
      parent = make_path(user, path)
    end

    if parent.nil?
      return nil
    end

    parent.children.new(name: file_name, full_path: full_path, is_directory: false, size: file_size)
  end

  def self.init_user(user)
    FileMetadata.new(name: user, full_path: user, is_directory: true, size: nil)
  end

  def self.user_path(user, path)
    if path.blank?
      user
    else
      "#{user}/#{path}"
    end
  end

  def self.full_path(user, path, dir_name)
    if path.blank?
      "#{user}/#{dir_name}"
    else
      "#{user}/#{path}/#{dir_name}"
    end
  end

  def self.free_space(user)
    taken_space = FileMetadata.user_files(user).map(&:size).reduce(0, :+)

    max_user_space - taken_space
  end

  # @return
  def self.max_user_space
    ENV['USER_SPACE'].to_i
  end

  def self.user_files(user)
    FileMetadata.where("full_path LIKE :prefix AND is_directory = false", prefix: "#{user}%")
  end

  # @return [FileMetadata]
  def self.get_file(user, path)
    FileMetadata.find_by_full_path(user_path(user, path))
  end

  def self.file_exists(user, path)
    get_file(user, path).present?
  end

  def self.user_exists(user)
    file_exists(user, "")
  end

  private

  def should_have_unique_path
    existing = FileMetadata.find_by_full_path(full_path)

    if existing.present?
      segments = existing.full_path.split("/", 2)

      errors.add(:full_path, "#{segments[1]} already exists")
    end
  end

  private

  # @param path [String]
  # @param file_name [String]
  def self.relative_path(path, file_name)
    if path.empty?
      file_name
    else
      "#{path}/#{file_name}"
    end
  end

  def should_have_enough_space
    return if size.nil?

    user = full_path.split("/").first

    if !is_directory && FileMetadata.free_space(user) < size
      errors.add(:size, "does not fit in user free space")
    end
  end
end