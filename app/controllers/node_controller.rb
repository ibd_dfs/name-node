class NodeController < ApplicationController
  def register_node
    node = Node.find_by_private_address(node_params[:private_address])

    node.delete if node.present?

    node = Node.new(node_params)
    node.last_alive = DateTime.now

    if node.save
      head :ok
    else
      render json: node.errors.full_messages, status: :unprocessable_entity
    end
  end

  def file_created
    address = params[:private_address]
    full_path = params[:full_path]
    is_replica = params[:is_replica]

    node = Node.find_by_private_address(address)
    file = FileMetadata.find_by_full_path(full_path)

    if node.nil? || file.nil?
      head :not_found
      return
    end
    replica = Replica.new(node_id: node.id, file_metadata_id: file.id)

    if replica.save
      head :ok
    else
      render json: replica.errors.full_messages, status: :unprocessable_entity
    end

    propagate_replication(node, file)

    replicate_file(node, file) unless is_replica
  end

  def health_check
    address = params[:private_address]

    node = Node.find_by_private_address(address)

    if node.nil?
      head :not_found
      return
    end

    node.last_alive = DateTime.now
    node.save!
  end

  def file_created_params
    params.permit(:address, :full_path)
  end

  def node_params
    params.permit(:public_address, :private_address, :total_volume)
  end

  private

  # @param file [FileMetadata]
  def replicate_file(node, file)
    candidates = Node.replicate_candidates(file)

    candidates.each do |candidate|
      communicator  = StorageNodeCommunicator.new(candidate.private_address, logger)
      communicator.replicate_file(node.private_address, file.full_path)
    end
  end

  def propagate_replication(node, file)
    parent = file.parent
    node_files = node.files

    while parent != nil && !parent.in?(node_files)
      Replica.create!(node_id: node.id, file_metadata_id: parent.id)

      parent = parent.parent
    end
  end
end
