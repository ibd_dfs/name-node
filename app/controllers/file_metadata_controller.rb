class FileMetadataController < ApplicationController
  before_action :set_file, only: [:file_info, :ls, :read_file, :delete]
  before_action :require_not_directory, only: [:read_file]
  before_action :set_file_node, only: [:read_file]

  def init_user
    user = params[:user]

    old_dir = FileMetadata.get_file(user, "")

    if old_dir.present?
      success = delete_file(old_dir)

      unless success
        render_unexpected_storage_error
        return
      end
    end

    directory = FileMetadata.init_user(user)

    if directory.save
      render json: {space_size: FileMetadata.max_user_space}
    else
      render json: directory.errors.full_messages, status: :unprocessable_entity
    end
  end

  def file_info; end

  def ls; end

  def mk
    user = params[:user]
    path = params[:path]
    dir = params[:dir_name]

    directory = FileMetadata.new_directory(user, path, dir)

    if directory.nil?
      render_user_not_found(user)

      return
    end

    if directory.save
      render json: {}, status: :ok
    else
      render json: directory.errors.full_messages, status: :unprocessable_entity
    end
  end

  def create_file
    user = params[:user]
    path = params[:path]
    filename = params[:name]

    file = FileMetadata.create_empty_file(user, path, filename)

    if file.nil?
      render_user_not_found(user)

      return
    end

    unless file.valid?
      render json: file.errors.full_messages, status: :unprocessable_entity
      return
    end

    node = Node.find_for_new_file(file)

    if node.nil?
      render_insufficient_storage
      return
    end

    file.save!

    communicator = StorageNodeCommunicator.new(node.private_address, logger)

    success = communicator.send_create_file(file.full_path)

    if success
      render json: {}, status: :ok
    else
      file.delete

      render_unexpected_storage_error
    end
  end

  def write_file
    user = params[:user]
    path = params[:path]
    filename = params[:name]
    size = params[:size]

    file = FileMetadata.create_file(user, path, filename, size)

    if file.nil?
      render_user_not_found(user)

      return
    end

    unless file.valid?
      render json: file.errors.full_messages, status: :unprocessable_entity
      return
    end

    @node = Node.find_for_new_file(file)

    if @node.nil?
      render_insufficient_storage
      return
    end

    file.save!
  end

  def move_file
    user = params[:user]
    user_relative_path_from = params[:path_from]
    user_relative_path_to = params[:path_to]

    full_path_from = FileMetadata.user_path(user, user_relative_path_from)
    full_path_to = FileMetadata.user_path(user, user_relative_path_to)

    file_from = FileMetadata.get_file(user, user_relative_path_from)

    if file_from.nil?
      render_path_not_found(full_path_from)

      return
    end

    if file_from.is_directory
      render_not_file(user_relative_path_from)

      return
    end

    replicas = file_from.replicas

    if replicas.empty?
      render_no_node_associated(full_path_from)

      return
    end

    file_name_to = user_relative_path_to.split("/").last
    path_to = user_relative_path_to.delete_suffix("/#{file_name_to}")

    if path_to == file_name_to #nothing was deleted => empty relative path
      path_to = ""
    end

    file_to = FileMetadata.create_file(user, path_to, file_name_to, file_from.size)

    unless file_to.valid?
      render json: file_to.errors.full_messages, status: :unprocessable_entity
      return
    end

    file_from.replicas.each do |replica|
      communicator = StorageNodeCommunicator.new(replica.node.private_address, logger)

      communicator.file_move(full_path_from, full_path_to)
    end

    file_to.replicas = file_from.replicas

    file_to.save!
    file_from.delete

    render json: {}, status: :ok
  end

  def copy_file
    user = params[:user]
    user_relative_path_from = params[:path_from]
    user_relative_path_to = params[:path_to]

    full_path_from = FileMetadata.user_path(user, user_relative_path_from)
    full_path_to = FileMetadata.user_path(user, user_relative_path_to)

    file_from = FileMetadata.get_file(user, user_relative_path_from)

    if file_from.nil?
      render_path_not_found(user_relative_path_from)

      return
    end

    if file_from.is_directory
      render_not_file(user_relative_path_from)

      return
    end

    node_from = file_from.source_node

    if node_from.nil?
      render_no_node_associated(user_relative_path_from)

      return
    end

    file_name_to = user_relative_path_to.split("/").last
    path_to = user_relative_path_to.delete_suffix("/#{file_name_to}")

    if path_to == file_name_to #nothing was deleted => empty relative path
      path_to = ""
    end

    file_to = FileMetadata.create_file(user, path_to, file_name_to, file_from.size)

    unless file_to.valid?
      render json: file_to.errors.full_messages, status: :unprocessable_entity
      return
    end

    node_to = Node.find_for_new_file(file_to)

    if node_to.nil?
      render_insufficient_storage

      return
    end

    communicator = StorageNodeCommunicator.new(node_to.private_address, logger)
    success = communicator.copy_file_from_neighbour(node_from.private_address, full_path_from, full_path_to)

    if success
      file_to.save!

      render json: {}, status: :ok
    else
      render_unexpected_storage_error
    end
  end

  def delete
    success = delete_file(@file)

    if success
      render json: {}, status: :ok
    else
      render_unexpected_storage_error
    end
  end

  def read_file; end

  private

  def delete_file(file)
    global_success = true

    file.replicas.each do |replica|
      communicator = StorageNodeCommunicator.new(replica.node.private_address, logger)

      if file.is_directory
        success = communicator.dir_delete(file.full_path)
      else
        success = communicator.file_delete(file.full_path)
      end

      if success
        replica.delete
      else
        global_success = false
      end
    end

    file.delete if global_success

    global_success
  end

  def set_file
    user = params[:user]
    path = params[:path]

    @file = FileMetadata.get_file(user, path)

    render_path_not_found(path) if @file.nil?
  end

  def require_not_directory
    render_not_file(path) if @file.is_directory
  end

  def set_file_node
    @node = @file.source_node

    render_no_node_associated(path) if @node.nil?
  end

  def render_user_not_found(user)
    render json: ["Storage for #{user} is not initialized"], status: :not_found
  end

  def render_path_not_found(path)
    render json: ["Path #{path} does not exist"], status: :not_found
  end

  def render_no_node_associated(path)
    render json: ["File #{path} has not been uploaded yet"], status: :not_found
  end

  def render_not_file(path)
    render json: ["#{path} is a directory, not a file"], status: :unprocessable_entity
  end

  def render_insufficient_storage
    render json: ["Unable to upload the file. Remote insufficient storage"], status: :insufficient_storage
  end

  def render_unexpected_storage_error
    render json: ["Unexpected storage error"], status: :internal_server_error
  end
end
