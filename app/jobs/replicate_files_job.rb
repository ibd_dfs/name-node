class ReplicateFilesJob < ApplicationJob
  queue_as :default

  def perform(*args)
    node_id = args[0]

    node = Node.find(node_id)

    node.files.where(is_directory: false).each do |file|
      candidate = Node.replicate_candidates(file).first

      another_source = file.replicas.where("node_id != ?", node.id).first

      if candidate.present? && another_source.present?
        communicator = StorageNodeCommunicator.new(candidate.address, Rails.logger)

        communicator.replicate_file(another_source.private_address, file.full_path)
      end
    end
  end
end
